package org.green.ipf.clean.master.main;

import org.green.ipf.clean.activemq.service.ActiveMqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.green.ipf.clean.elastic.service.ElasticsearchService;
import org.green.ipf.cassandra.client.service.CassandraClientService;

@Component
public class AllCleaner implements CommandLineRunner {
    @Autowired
    private ElasticsearchService elasticsearchService;

    @Autowired
    private CassandraClientService cassandraClientService;

    @Autowired
    private ActiveMqService activeMqService;

    @Override
    public void run(String... args) throws Exception {
        cassandraClientService.dropKeyspace("ipf");
        cassandraClientService.dropKeyspace("ipf_cql");
        cassandraClientService.destroy();
        activeMqService.purgeAllQueue("localhost", "61616");
        elasticsearchService.deleteTemplates("localhost", "9200");
        elasticsearchService.deleteIndices("localhost", "9200");
        System.exit(1);
    }
}
