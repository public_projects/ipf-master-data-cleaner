package org.green.ipf.clean.master;

import org.green.ipf.cassandra.client.main.CassandraCleanMain;
import org.green.ipf.clean.activemq.main.CleanActiveMQMain;
import org.green.ipf.clean.elastic.main.ElasticSearchCleanMain;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@ComponentScan(basePackages = {
        "org.green.ipf.clean.elastic",
        "org.green.ipf.clean.activemq",
        "org.green.ipf.cassandra.client",
        "org.green.ipf.clean.master"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = ElasticSearchCleanMain.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CassandraCleanMain.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CleanActiveMQMain.class)}
)
public class IpfMasterDataCleanerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IpfMasterDataCleanerApplication.class, args);
    }

}
