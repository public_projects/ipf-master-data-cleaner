@echo off
echo Setting JAVA_HOME
set JAVA_HOME=C:\software\Java\jdk-17.0.6
echo setting PATH
set PATH=C:\software\Java\jdk-17.0.6\bin;%PATH%
echo Display java version
java -version
set SPRING_PROFILES_ACTIVE=LOCAL
call mvn spring-boot:run
pause
